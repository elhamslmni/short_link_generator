from django.urls import path

from . import views

app_name = 'generator'
urlpatterns = [
    path('<str:short_url>/', views.ShortLinkView.as_view(), name='short_link_page'),
    path('', views.ShortLinKGenerator.as_view(), name='short_link_generator_page'),
    path('v/<str:short_url>/', views.LinkVisitedNumberView.as_view(), name='visited_number_page'),

]
