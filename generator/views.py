import json
from http import HTTPStatus
from random import randint

from django.shortcuts import redirect
from django.http import JsonResponse
from django.views import View

from .models import Url
from generator.static_data import (
    LINK,
    SHORT_LINK,
    BASE_ADDRESS,
    VISITED_NUMBER
)


class ShortLinKGenerator(View):
    def get(self, request):
        link = json.loads(request.body)[LINK]

        url = Url.objects.create(original_link=link)

        short_pass = url.short_pass
        return JsonResponse(
            {
                SHORT_LINK: BASE_ADDRESS + short_pass,
            },
            status=HTTPStatus.CREATED
        )


class ShortLinkView(View):
    def get(self, request, short_url):
        try:
            url = Url.objects.get(short_pass=short_url)
            url.visit_number = url.visit_number + 1
            url.save()
            return redirect(url.original_link)
        except:
            return JsonResponse({}, status=HTTPStatus.NOT_FOUND)


class LinkVisitedNumberView(View):
    def get(self, request, short_url):
        try:
            url = Url.objects.get(short_pass=short_url)
            return JsonResponse(
                {
                    VISITED_NUMBER: url.visit_number
                }, status=HTTPStatus.OK)
        except:
            return JsonResponse({}, status=HTTPStatus.NOT_FOUND)
