from django.db import models


class Url(models.Model):
    original_link = models.CharField(max_length=200)
    short_pass = models.CharField(max_length=50, blank=True)
    visit_number = models.IntegerField(default=0)
