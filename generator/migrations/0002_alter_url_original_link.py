# Generated by Django 3.2.14 on 2022-11-09 14:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('generator', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='url',
            name='original_link',
            field=models.CharField(max_length=200, unique=True),
        ),
    ]
