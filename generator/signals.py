import random
import string

from django.dispatch import receiver
from django.db.models.signals import post_save

from generator.models import Url


@receiver(post_save, sender=Url)
def create_post(instance, created, **kwargs):
    if created:
        new_url = instance
        random_string = ''.join(random.choices(string.ascii_lowercase, k=2))
        short_pass = f"{new_url.id:x}_{random_string}"
        new_url.short_pass = short_pass
        new_url.save()
