
This is a Django project for shorten links

#### postgres

Use 
```bash
docker-compose up 
pip install -r requirements.txt
```


#### Django Web Server

```bash
python3 manage.py migrate
python3 manage.py runserver
```





#### Postman

1. Open the postman collection.
2. Send the request `` and send json request with this method -> : "link" : "your url" and get short url.
3. send request to short url 
4. send request to '/v/{short address}' to see visited number
